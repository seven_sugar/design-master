package com.example.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.dao.SysProjectMapper;
import com.example.demo.dao.SysUserMapper;
import com.example.demo.entity.Result;
import com.example.demo.entity.SysProject;
import com.example.demo.entity.SysUser;
import com.example.demo.vo.SysProjectVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/project")
public class ProjectController {
    @Resource
    private SysProjectMapper projectMapper;

    @PostMapping("/list")
    public Result<List<SysProject>> list(@RequestBody SysProjectVo vo) {
        Result<List<SysProject>> result=new Result<>();

        List<SysProject> list = projectMapper.list(vo);
        result.setCode(200);
        result.setMessage("成功");
        result.setData(list);
        return result;
    }

    @PostMapping("/edit")
    public Result edit(@RequestBody SysProjectVo project){
        Result result=new Result<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SysProject sysProject = new SysProject();
        try {
            if(project.getId() == null){
                sysProject.setName(project.getName());
                sysProject.setEngineer(project.getEngineer());
                sysProject.setAmount(project.getAmount());
                sysProject.setStartTime(sdf.parse(project.getStartTime()));
                sysProject.setEndTime(sdf.parse(project.getEndTime()));
                sysProject.setApprovalStatus(0);
                sysProject.setDeleted(0);
                sysProject.setCreateTime(new Date());
                projectMapper.insert(sysProject);
            }else{
                sysProject = projectMapper.selectById(project.getId());
                sysProject.setName(project.getName());
                sysProject.setEngineer(project.getEngineer());
                sysProject.setAmount(project.getAmount());
                sysProject.setStartTime(sdf.parse(project.getStartTime()));
                sysProject.setEndTime(sdf.parse(project.getEndTime()));
                sysProject.setUpdateTime(new Date());
                projectMapper.updateById(sysProject);
            }
            result.setCode(200);
            result.setMessage("操作成功");
            return result ;
        } catch (ParseException e) {
            result.setCode(0);
            result.setMessage("系统异常，操作失败");
            return result ;
        }
    }

    @PostMapping("/audit")
    public Result audit(@RequestBody SysProjectVo project){
        Result result=new Result<>();
        if(project.getId() == null){
            result.setCode(0);
            result.setMessage("数据异常，操作失败");
            return result ;
        }
        SysProject sysProject = projectMapper.selectById(project.getId());
        sysProject.setApprovalStatus(1);
        sysProject.setApprovalResult(project.getApprovalResult());
        sysProject.setApprovalContent(project.getApprovalContent());
        sysProject.setApprovalTime(new Date());
        sysProject.setUpdateTime(new Date());
        projectMapper.updateById(sysProject);
        result.setCode(200);
        result.setMessage("操作成功");
        return result ;
    }

    @PostMapping("/delete")
    public Result delete(@RequestBody SysProjectVo project){
        Result result=new Result<>();
        if(project.getId() == null){
            result.setCode(0);
            result.setMessage("数据异常，操作失败");
            return result ;
        }
        SysProject sysProject = projectMapper.selectById(project.getId());
        sysProject.setDeleted(1);
        sysProject.setUpdateTime(new Date());
        projectMapper.updateById(sysProject);
        result.setCode(200);
        result.setMessage("操作成功");
        return result ;
    }
}
