package com.example.demo.controller;

import com.example.demo.dao.SysProjectMapper;
import com.example.demo.dao.SysUserMapper;
import com.example.demo.entity.Result;
import com.example.demo.entity.SysProject;
import com.example.demo.entity.SysUser;
import com.example.demo.vo.SysProjectVo;
import com.example.demo.vo.SysUserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.sql.Wrapper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private SysUserMapper sysUserMapper;

    @PostMapping("/list")
    public Result<List<SysUserVo>> list(@RequestBody SysUserVo vo) {
        Result<List<SysUserVo>> result = new Result<>();
        List<SysUserVo> list = sysUserMapper.getUserList(vo);
        result.setCode(200);
        result.setMessage("成功");
        result.setData(list);
        return result;
    }

    @PostMapping("/edit")
    public Result edit(@RequestBody SysUserVo sysUserVo) {
        Result result = new Result<>();
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(sysUserVo, sysUser);
        if (sysUserVo.getId() == null) {
            sysUser.setDeleted(0);
            sysUser.setCreateTime(new Date());
            sysUserMapper.insert(sysUser);
        } else {
            if (sysUser.getId() == 1 && !("admin".equals(sysUser.getLoginName()))) {
                result.setCode(-1);
                result.setMessage("不允许修改超级管理员用户名!");
                return result;
            }
            sysUserMapper.updateById(sysUser);
        }
        result.setCode(200);
        result.setMessage("操作成功");
        return result;
    }


    @PostMapping("/delete")
    public Result delete(@RequestBody SysUserVo sysUserVo) {
        Result result = new Result<>();
        if (sysUserVo.getId() == null) {
            result.setCode(0);
            result.setMessage("数据异常，操作失败");
            return result;
        }
        SysUser sysUser= sysUserMapper.selectById(sysUserVo.getId());
        sysUser.setDeleted(1);
        sysUserMapper.updateById(sysUser);
        result.setCode(200);
        result.setMessage("操作成功");
        return result;
    }
}
