package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;


@Data
public class SysProjectVo{

    private Integer id;

    private String name;

    private String engineer;

    private Integer approvalStatus;

    private Integer approvalResult;

    private String approvalContent;

    private Double amount;

    private String startTime;

    private String endTime;

    private Integer page;

    private Integer limit;

}
