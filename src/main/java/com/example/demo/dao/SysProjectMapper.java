package com.example.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.SysProject;
import com.example.demo.vo.SysProjectVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface SysProjectMapper extends BaseMapper<SysProject> {

    List<SysProject> list(@Param("param")SysProjectVo vo);
}
