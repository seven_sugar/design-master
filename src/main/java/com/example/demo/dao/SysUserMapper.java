package com.example.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.SysUser;
import com.example.demo.vo.SysUserVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface SysUserMapper extends BaseMapper<SysUser> {

    @Select("select * from sys_user where deleted = 0")
    List<SysUserVo> getUserList(SysUserVo vo);
}
