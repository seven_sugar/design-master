package com.example.demo.entity;

import lombok.Data;

@Data
public class Result<T> {
    /** 状态码 */
    private Integer code;
    /** 提示信息  */
    private String message;
    /** 具体数据 */
    private T data;
}
