-- user用户表
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) DEFAULT NULL COMMENT '名字',
  `login_name` varchar(32) DEFAULT NULL COMMENT '登录名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `deleted` int(11) DEFAULT 0 COMMENT '逻辑删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- 工程表
CREATE TABLE `sys_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '工程名',
  `engineer` varchar(255) DEFAULT NULL COMMENT '人员',
  `amount` double(11,2) DEFAULT NULL COMMENT '金额',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `approval_status` int(11) DEFAULT NULL COMMENT '审核状态',
  `approval_result` int(11) DEFAULT NULL COMMENT '审核结果',
  `approval_content` varchar(255) DEFAULT NULL COMMENT '审核内容',
  `approval_user` varchar(255) DEFAULT NULL COMMENT '审核人员',
  `approval_time` datetime DEFAULT NULL COMMENT '审核时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `deleted` int(11) DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;